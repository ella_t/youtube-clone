import React from "react";
import "./thumbnail.css";
import { MoreVert } from "@material-ui/icons";

function Thumbnail() {
  return (
    <div className="thumbnail">
      <div className="thumbnailContainer">
        <div className="thumbnailCard">
          <img
            src="https://images.pexels.com/photos/1595387/pexels-photo-1595387.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
            alt="Thumbnail"
            className="thumbnailImg"
          />
          <div className="thumbnailCardInfo">
            <div className="thumbnailImgContainer">
              <img
                src="https://images.pexels.com/photos/1595387/pexels-photo-1595387.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
                alt="Thumbnail"
                className="thumbnailCardImg"
              />
              <div className="thumbnailDescriptor">
                <span className="thumbnailCardTxt">
                  Ariana Grande Ariana Grande
                </span>
                {/* <div className="toast">Lama Deva</div> */}
                <span className="channelName">Lama Deva</span>
                <div className="likeTimeAgoContainer">
                  <span className="like">1234 Likes</span>
                  <span className="dot">.</span>
                  <span className="timeAgo"> 4 hours ago</span>
                </div>
              </div>
            </div>
            <MoreVert className="thumbnailCardIcon" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Thumbnail;
