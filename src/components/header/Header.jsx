import React, { useContext, useState } from "react";
import "./header.css";
import {
  YouTube,
  Menu,
  Search,
  Mic,
  VideoCall,
  Apps,
  Notifications,
  AccountBox,
  MonetizationOn,
  SettingsApplications,
  ExitToApp,
  PermMedia,
  MoreVert,
  AccountCircle
} from "@material-ui/icons";
import { AuthContext } from "../../context/AuthContext";
import {Link} from 'react-router-dom'
function Header() {
  const [visible, setVisible] = useState(false);
  const { user } = useContext(AuthContext);
  return (
    <div className="header">
      <div className="headerLeft">
        <Menu className="headerLeftIcon" style={{ fontSize: 25 }} />
        <div className="headerLeftLogoContainer">
          <YouTube
            className="headerLeftLogoIcon"
            style={{ fontSize: 25, color: "red" }}
          />
          <h4 className="headerLeftText">Youtube</h4>
        </div>
      </div>
      <div className="headerCenter">
        <div className="headerCenterContainer">
          <input
            type="text"
            placeholder="Serch"
            className="headerCenterSearch"
          />
          <button className="searchBtn">
            <Search
              htmlColor="red"
              style={{ fontSize: 25 }}
              className="headerCenterSearchIcon"
            />
          </button>
        </div>
        <Mic className="headerCenterIcon" style={{ fontSize: 25 }} />
      </div>
      <div className="headerRight">
        {
          user ? 
          <>  
          <VideoCall className="headerRightIcon" style={{ fontSize: 25 }} />
          <Apps className="headerRightIcon" style={{ fontSize: 25 }} />
          <Notifications className="headerRightIcon" style={{ fontSize: 25 }} />
          <img
            src="https://images.pexels.com/photos/8379310/pexels-photo-8379310.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
            alt=""
            className="headerRightProfileImg"
            onClick={() => {
              setVisible(!visible);
              console.log("Visible: ", visible);
            }}
          />
          <div className={visible ? "popProfile" : "popProfile hide"}>
            <div className="popProfileDescriptor">
              <img
                src="https://images.pexels.com/photos/8379310/pexels-photo-8379310.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940"
                alt=""
                className="popProfileImg"
              />
              <div className="popProfileInfo">
                <h4 className="popProfileName">Ermias Tefera</h4>
                <span className="popProfileDesc">Manage Your Google Account</span>
              </div>
            </div>
            <div className="popProfileMenu">
              <div className="popProfileItems">
                <div className="popProfileItem">
                  <AccountBox className="popProfileIcon" style={{ fontSize: 25 }} />
                  <span className="popProfileMenuTxt">Your Channel</span>
                </div>
                <div className="popProfileItem">
                  <MonetizationOn
                    className="popProfileIcon"
                    style={{ fontSize: 25 }}
                  />
                  <span className="popProfileMenuTxt">Purchas and memberships</span>
                </div>
                <div className="popProfileItem">
                  <SettingsApplications
                    className="popProfileIcon"
                    style={{ fontSize: 25 }}
                  />
                  <span className="popProfileMenuTxt">YouTube Studio</span>
                </div>
                <div className="popProfileItem">
                  <PermMedia className="popProfileIcon" style={{ fontSize: 25 }} />
                  <span className="popProfileMenuTxt">Switch Account</span>
                </div>
                <div className="popProfileItem">
                  <ExitToApp className="popProfileIcon" style={{ fontSize: 25 }} />
                  <span className="popProfileMenuTxt">Sign Out</span>
                </div>
              </div>
            </div>
            <div className="popProfileSettings">
              <div className="popProfileItem">
                <AccountBox className="popProfileIcon" style={{ fontSize: 25 }} />
                <span className="popProfileMenuTxt">Appearnce: Device Theme</span>
              </div>
              <div className="popProfileItem">
                <AccountBox className="popProfileIcon" style={{ fontSize: 25 }} />
                <span className="popProfileMenuTxt">Language: English</span>
              </div>
              <div className="popProfileItem">
                <AccountBox className="popProfileIcon" style={{ fontSize: 25 }} />
                <span className="popProfileMenuTxt">Location: United States</span>
              </div>
              <div className="popProfileItem">
                <AccountBox className="popProfileIcon" style={{ fontSize: 25 }} />
                <span className="popProfileMenuTxt">Settings</span>
              </div>
              <div className="popProfileItem">
                <AccountBox className="popProfileIcon" style={{ fontSize: 25 }} />
                <span className="popProfileMenuTxt">Your Data on YouTube</span>
              </div>
              <div className="popProfileItem">
                <AccountBox className="popProfileIcon" style={{ fontSize: 25 }} />
                <span className="popProfileMenuTxt">Help</span>
              </div>
              <div className="popProfileItem">
                <AccountBox className="popProfileIcon" style={{ fontSize: 25 }} />
                <span className="popProfileMenuTxt">Send Feedback</span>
              </div>
              <div className="popProfileItem">
                <AccountBox className="popProfileIcon" style={{ fontSize: 25 }} />
                <span className="popProfileMenuTxt">Keyboard Shortcuts</span>
              </div>
            </div>
            <div className="popProfileItem">
              <span className="popProfileMenuTxt">
                Restricted Mode: <span className="mode">off</span>
              </span>
            </div>
          </div>
          </>
           : <> 
             <Apps className="headerRightIcon" style={{ fontSize: 25 }} />
             <MoreVert className="headerIcon" style={{fontSize:25}} />
             <Link to="/login" className="loginButton">
                <div className="signInButtonContainer">
                  <AccountCircle className="headerRightIcon" style={{fontSize: 25}} />
                  <button className="signInButton">Sign In</button>
                </div>
             </Link>
           </>
        }
      </div>
     
      <hr className="divider" />
    </div>
  );
}

export default Header;
