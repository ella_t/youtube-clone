import React from "react";
import "./category.css";
function Category() {
  return (
    <div className="category">
      <button className="categoryBtn">All</button>
      <button className="categoryBtn">Javascript</button>
      <button className="categoryBtn">Website</button>
      <button className="categoryBtn">Database</button>
    </div>
  );
}

export default Category;
