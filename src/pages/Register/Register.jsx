import React, { useRef } from "react";
import { Link } from "react-router-dom";
import "./register.css";
import axios from 'axios'

function Register() {
  const userNameRef = useRef();
  const emailRef = useRef();
  const passwordRef = useRef();
  const passwordAgainRef = useRef();

  const handleSubmit = async(e) => {
    e.preventDefault();
    if (passwordAgainRef.current.value !== passwordRef.current.value) {
      passwordAgainRef.current.setCustomValidity("Password Don't Match");
      // passwordAgainRef.reportValidity();
    } else {
      const user = {
         userName: userNameRef.current.value,
         email: emailRef.current.value,
        password: passwordRef.current.value
      }
      const res = await axios.post("/auth/register",user)
      console.log(res);
    }
  };
  
  return (
    <div className="register">
      <form className="registerContainer" onSubmit={handleSubmit}>
        <div className="registerDesc">
          <img
            src="https://www.freepnglogos.com/uploads/youtube-logo-png/new-youtube-logo-icon-update-youtube-issue-fortawesome-font-3.png"
            alt="YouTube Logo"
            className="logo"
          />
          <div className="logoDescriptor">
            <span className="signInTxt">Sign Up</span>
            <p className="logonDesc">to continue to YouTube</p>
          </div>
        </div>
        <input
          type="text"
          name="userName"
          className="userNameInput"
          placeholder="User Name"
          ref={userNameRef}
          required
        />
        <input
          type="email"
          name="email"
          className="emailInput"
          placeholder="Email"
          ref={emailRef}
          required
        />
        <input
          type="password"
          name="password"
          className="passwordInput"
          placeholder="Password"
          minLength="6"
          ref={passwordRef}
          required
        />
        <input
          type="password"
          name="passwordAgain"
          className="passwordAgainInput"
          placeholder="Password Again"
          ref={passwordAgainRef}
          required
        />
        <div className="createAccountContainer">
          <Link to="/login" className="createAccount">
            Sign In
          </Link>
          <button className="next" type="submit">
            Sign Up
          </button>
        </div>
      </form>
    </div>
  );
}

export default Register;
