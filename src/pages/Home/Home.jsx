import React, { useContext } from "react";
import "./home.css";
import Header from "../../components/header/Header";
import {
  Home as HomeIcon,
  Explore,
  Subscriptions,
  VideoLibrary,
  History,
  VideoLabel,
  WatchLater,
  ThumbUpAlt,
  ExpandMore,
  Person,
  SportsEsports,
  LiveTv,
  EmojiPeople,
  EmojiObjects,
  Sports,
} from "@material-ui/icons";
import { Link } from "react-router-dom";
import Category from "../../components/Category/Category";
import Thumbnail from "../../components/Thumbnail/Thumbnail";
import { AuthContext } from "../../context/AuthContext";

function Home() {
  const { user } = useContext(AuthContext);
  console.log(user);
  return (
    <>
      <Header />
      <div className="home">
        <div className="sidebar">
          <div className="sideBarMainMenu">
            <div className="mainMenuItems">
              <Link className="mainMenuLink">
                <div className="mainMenuItem">
                  <HomeIcon className="mainMenuIcon" style={{ fontSize: 20 }} />
                  <span className="mainMenuText">Home</span>
                </div>
              </Link>
              <Link className="mainMenuLink">
                <div className="mainMenuItem">
                  <Explore className="mainMenuIcon" style={{ fontSize: 20 }} />
                  <span className="mainMenuText">Explore</span>
                </div>
              </Link>
              <Link className="mainMenuLink">
                <div className="mainMenuItem">
                  <Subscriptions
                    className="mainMenuIcon"
                    style={{ fontSize: 20 }}
                  />
                  <span className="mainMenuText">Subscriptions</span>
                </div>
              </Link>
            </div>
          </div>

          <div className="subMenu">
            <Link className="mainMenuLink">
              <div className="mainMenuItem">
                <VideoLibrary
                  className="mainMenuIcon"
                  style={{ fontSize: 20 }}
                />
                <span className="mainMenuText">Library</span>
              </div>
            </Link>
            <Link className="mainMenuLink">
              <div className="mainMenuItem">
                <History className="mainMenuIcon" style={{ fontSize: 20 }} />
                <span className="mainMenuText">History</span>
              </div>
            </Link>
            <Link className="mainMenuLink">
              <div className="mainMenuItem">
                <VideoLabel className="mainMenuIcon" style={{ fontSize: 20 }} />
                <span className="mainMenuText">Your Videos</span>
              </div>
            </Link>
            <Link className="mainMenuLink">
              <div className="mainMenuItem">
                <WatchLater className="mainMenuIcon" style={{ fontSize: 20 }} />
                <span className="mainMenuText">Watch Later</span>
              </div>
            </Link>
            <Link className="mainMenuLink">
              <div className="mainMenuItem">
                <ThumbUpAlt className="mainMenuIcon" style={{ fontSize: 20 }} />
                <span className="mainMenuText">Liked Videos</span>
              </div>
            </Link>
            <Link className="mainMenuLink">
              <div className="mainMenuItem">
                <ExpandMore className="mainMenuIcon" style={{ fontSize: 20 }} />
                <span className="mainMenuText">Show More</span>
              </div>
            </Link>
          </div>
          <div className="subscriptionContainer">
            <h4 className="subscriptionTitle">Subscriptions</h4>
            <div className="subscriptionList">
              <Link className="mainMenuLink">
                <div className="mainMenuItem">
                  <Person className="mainMenuIcon" style={{ fontSize: 20 }} />
                  <span className="mainMenuText">Rithm School</span>
                </div>
              </Link>
              <Link className="mainMenuLink">
                <div className="mainMenuItem">
                  <VideoLibrary
                    className="mainMenuIcon"
                    style={{ fontSize: 20 }}
                  />
                  <span className="mainMenuText">Lama Deva</span>
                </div>
              </Link>
              <Link className="mainMenuLink">
                <div className="mainMenuItem">
                  <VideoLibrary
                    className="mainMenuIcon"
                    style={{ fontSize: 20 }}
                  />
                  <span className="mainMenuText">Divinector</span>
                </div>
              </Link>
              <Link className="mainMenuLink">
                <div className="mainMenuItem">
                  <VideoLibrary
                    className="mainMenuIcon"
                    style={{ fontSize: 20 }}
                  />
                  <span className="mainMenuText">Academind</span>
                </div>
              </Link>
              <Link className="mainMenuLink">
                <div className="mainMenuItem">
                  <VideoLibrary
                    className="mainMenuIcon"
                    style={{ fontSize: 20 }}
                  />
                  <span className="mainMenuText">Traversy Media</span>
                </div>
              </Link>
            </div>
          </div>
          <div className="moreYoutubeContainer">
            <h4 className="subscriptionTitle">More From Youtube</h4>
            <div className="subscriptionList">
              <Link className="mainMenuLink">
                <div className="mainMenuItem">
                  <SportsEsports
                    className="mainMenuIcon"
                    style={{ fontSize: 20 }}
                  />
                  <span className="mainMenuText">Gaming</span>
                </div>
              </Link>
              <Link className="mainMenuLink">
                <div className="mainMenuItem">
                  <LiveTv className="mainMenuIcon" style={{ fontSize: 20 }} />
                  <span className="mainMenuText">Live</span>
                </div>
              </Link>
              <Link className="mainMenuLink">
                <div className="mainMenuItem">
                  <EmojiPeople
                    className="mainMenuIcon"
                    style={{ fontSize: 20 }}
                  />
                  <span className="mainMenuText">Fashin and Beauty</span>
                </div>
              </Link>
              <Link className="mainMenuLink">
                <div className="mainMenuItem">
                  <EmojiObjects
                    className="mainMenuIcon"
                    style={{ fontSize: 20 }}
                  />
                  <span className="mainMenuText">Learning</span>
                </div>
              </Link>
              <Link className="mainMenuLink">
                <div className="mainMenuItem">
                  <Sports className="mainMenuIcon" style={{ fontSize: 20 }} />
                  <span className="mainMenuText">Sports</span>
                </div>
              </Link>
            </div>
          </div>
        </div>
        <div className="playerSection">
          <Category />
          <div className="container">
            <div className="thumbnailContainer">
              <Thumbnail />
              <Thumbnail />
              <Thumbnail />
              <Thumbnail />
              <Thumbnail />
              <Thumbnail />
              <Thumbnail />
              <Thumbnail />
              <Thumbnail />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Home;
