import React, { useContext, useRef } from "react";
import { Link } from "react-router-dom";
import { loginCall } from "../../apiCalls";
import { AuthContext } from "../../context/AuthContext";
import "./login.css";
function Login() {
  const emailRef = useRef();
  const passwordRef = useRef();
  const {isFetching, user, error, dispatch} = useContext(AuthContext);
  const handleSubmit = (e) =>{
    e.preventDefault();
    loginCall({email: emailRef.current.value,password: passwordRef.current.value},dispatch)
  };
  console.log(user);

  return (
    <div className="login">
      <form className="loginContainer" onSubmit={handleSubmit}>
        <div className="loginDesc">
          <img
            src="https://www.freepnglogos.com/uploads/youtube-logo-png/new-youtube-logo-icon-update-youtube-issue-fortawesome-font-3.png"
            alt="YouTube Logo"
            className="logo"
          />
          <div className="logoDescriptor">
            <span className="signInTxt">Sign In</span>
            <p className="logonDesc">to continue to YouTube</p>
          </div>
        </div>
        <input
          type="text"
          name="email"
          className="emailInput"
          placeholder="Email"
          ref={emailRef}
        />
        <input
          type="text"
          name="password"
          className="passwordInput"
          placeholder="Password"
          ref={passwordRef}
        />
        <p className="forgotEmail">Forgot Account?</p>
        <p className="guestMode">
          Not your computer? Use Guest mode to sign in privately.
          <Link to="#" className="learnMore">
            Learn more
          </Link>
        </p>
        <div className="createAccountContainer">
          <Link to="/register" className="createAccount">
            Create Account
          </Link>
          <button className="next" type="submit">Sign In</button>
        </div>
      </form>
    </div>
  );
}

export default Login;
