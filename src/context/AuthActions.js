export const LoginStart = (user) => ({
    type: "LOGIN_START"
});

export const LoginSuccess = (user) => ({
    type: "LOGIN_SUCCESS"
});

export const LoginFailure= (user) => ({
    type: "LOGIN_FAILURE"
});