import './App.css';
import Register from './pages/Register/Register';
import Home from './pages/Home/Home';
import About from './pages/About/About'
import Login from './pages/Login/Login'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom'
import { useContext } from 'react';
import { AuthContext } from './context/AuthContext';


function App() {
  const { user } = useContext(AuthContext);
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path='/'>
            {user ? <Home /> : <Login />}  
          </Route>
          <Route path="/login">
            { user ? <Redirect to="/" /> : <Login />}
          </Route>
          <Route path="/register">
            {user ? <Redirect to="/" /> : <Register />}
          </Route>
          <Route path='/about'>
            {user ? <About /> : <About /> } 
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
